package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Pages.AccountPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SignInTest {
    private WebDriver driver;
    private AccountPage accountPage;

    @BeforeClass
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        accountPage = PageFactory.initElements(driver, AccountPage.class);

    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test()
    void signInTestCase(){
        accountPage.signInWithFaceBook();
        Assert.assertTrue(accountPage.checkProfile(), "profile button isn't present");
    }

    @AfterClass()
    void afterTest() throws IOException {
        driver.quit();
    }
}