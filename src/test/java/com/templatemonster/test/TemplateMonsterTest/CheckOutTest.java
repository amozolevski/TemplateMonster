package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Pages.CheckOutPage;
import com.templatemonster.Pages.ProductPage;
import com.templatemonster.Services.User;
import com.templatemonster.Services.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class CheckOutTest {
    private WebDriver driver;
    private Utils utils;
    private ProductPage productPage;
    private CheckOutPage checkOutPage;

    @BeforeMethod
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        utils = new Utils(driver);
        productPage = PageFactory.initElements(driver, ProductPage.class);
        checkOutPage = PageFactory.initElements(driver, CheckOutPage.class);

    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @DataProvider
    Object[][] initUserCountry(){
        return new String[][]{
                {"US"},
                {"UA"}
        };
    }

    @Test(dataProvider = "initUserCountry")
    void CheckOutTestCase(String userCountry) throws IOException {
        productPage.addProductToCart();
        checkOutPage.registerBuyer(new User(userCountry));
        Assert.assertTrue(checkOutPage.getViaPayPalButton().isDisplayed(), "PayPal button isn't present");
    }

    @AfterMethod()
    void afterTest() throws IOException {
        utils.takeScreenShot();
        driver.quit();
    }
}