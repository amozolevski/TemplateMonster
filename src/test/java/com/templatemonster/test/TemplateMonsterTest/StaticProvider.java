package com.templatemonster.test.TemplateMonsterTest;

import org.testng.annotations.DataProvider;

public class StaticProvider {

   @DataProvider(name="langs")
   public static Object[][] langs(){
       return new String[][]{
               {"FR"},
               {"UA"},
       };
   }

    @DataProvider(name = "userCountry")
    public static Object[][] initUserCountry(){
        return new String[][]{
                {"US"},
                {"UA"}
        };
    }
}
