package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Services.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class CookieTest {
    private WebDriver driver;
    private Utils utils;

    @BeforeClass
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                                    DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        utils = new Utils(driver);
    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test
    void getCookieTestCase(){
        Cookie expectCookie = utils.getSomeCookie("aff");
        Assert.assertTrue(expectCookie.getValue().equals("TM"), "isn't expected cookie value");
    }

    @AfterClass()
    void afterTest() throws IOException {
        driver.quit();
    }
}