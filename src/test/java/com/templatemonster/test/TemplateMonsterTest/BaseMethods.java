package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Pages.AccountPage;
import com.templatemonster.Pages.CheckOutPage;
import com.templatemonster.Pages.MainPage;
import com.templatemonster.Pages.ProductPage;
import com.templatemonster.Services.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseMethods {
    private WebDriver driver;
    private Utils utils;
    private MainPage mainPage;
    private ProductPage productPage;
    private CheckOutPage checkOutPage;
    private AccountPage accountPage;

//    @Parameters({"browser"})
    @BeforeClass
    void beforeTest() throws MalformedURLException {
//    void beforeTest(String browser) throws MalformedURLException {
//        if(browser.equalsIgnoreCase("chrome")){
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                    DesiredCapabilities.chrome());
//        } else if(browser.equalsIgnoreCase("firefox")){
//            FirefoxProfile fp = new FirefoxProfile();
// set something on the profile...

//            DesiredCapabilities dc = DesiredCapabilities.firefox();
//            dc.setCapability(FirefoxDriver.PROFILE, fp);
//            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);

//            WebDriver driver = new RemoteWebDriver(dc);
//            System.setProperty("webdriver.firefox.marionette", "/opt/firefox/firefox");
//            System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
//            DesiredCapabilities dc = DesiredCapabilities.firefox();
//            dc.setBrowserName("firefox");
//            dc.setCapability("marionette", true);
//            dc.setPlatform(Platform.LINUX);
//            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);
//            driver = new FirefoxDriver();
//        }

        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        utils = new Utils(driver);
        mainPage = PageFactory.initElements(driver, MainPage.class);
        productPage = PageFactory.initElements(driver, ProductPage.class);
        checkOutPage = PageFactory.initElements(driver, CheckOutPage.class);
        accountPage = PageFactory.initElements(driver, AccountPage.class);
    }

    @AfterMethod()
    void afterTest() throws IOException {
        utils.takeScreenShot();
        driver.quit();
    }
}
