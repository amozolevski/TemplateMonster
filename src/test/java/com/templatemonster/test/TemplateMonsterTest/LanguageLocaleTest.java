package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Pages.MainPage;
import com.templatemonster.Services.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class LanguageLocaleTest {
    private WebDriver driver;
    private Utils utils;
    private MainPage mainPage;

    @BeforeClass
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        utils = new Utils(driver);
        mainPage = PageFactory.initElements(driver, MainPage.class);
    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @DataProvider
    Object[][] langs(){
        return new String[][]{
                {"FR"},
                {"UA"},
        };
    }

    @Test(dataProvider = "langs")
    void languageLocaleTestCase(String language){
        mainPage.selectLanguage(language);
        Assert.assertTrue(utils.checkSomeUrl(language), "it isn't expected URL");
    }

    @AfterClass()
    void afterTest() throws IOException {
        driver.quit();
    }
}