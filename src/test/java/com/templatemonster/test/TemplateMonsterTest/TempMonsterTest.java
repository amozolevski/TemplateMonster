package com.templatemonster.test.TemplateMonsterTest;

import com.templatemonster.Pages.AccountPage;
import com.templatemonster.Pages.CheckOutPage;
import com.templatemonster.Pages.MainPage;
import com.templatemonster.Pages.ProductPage;
import com.templatemonster.Services.User;
import com.templatemonster.Services.Utils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TempMonsterTest extends BaseMethods{
    private WebDriver driver;
    private Utils utils;
    private MainPage mainPage;
    private ProductPage productPage;
    private CheckOutPage checkOutPage;
    private AccountPage accountPage;


    //!!!!!!!!!!!!READ https://www.mkyong.com/unittest/testng-tutorial-5-suite-test/
    //
    //
    //
//    @Parameters ({"browser"})
//    @BeforeMethod
//    void beforeTest(String browser) throws MalformedURLException {
//        if(browser.equalsIgnoreCase("chrome")){
//            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
//                    DesiredCapabilities.chrome());
//        } else if(browser.equalsIgnoreCase("firefox")){
//            FirefoxProfile fp = new FirefoxProfile();
//// set something on the profile...
//
//            DesiredCapabilities dc = DesiredCapabilities.firefox();
//            dc.setCapability(FirefoxDriver.PROFILE, fp);
//            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);
//
////            WebDriver driver = new RemoteWebDriver(dc);
////            System.setProperty("webdriver.firefox.marionette", "/opt/firefox/firefox");
////            System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
////            DesiredCapabilities dc = DesiredCapabilities.firefox();
////            dc.setBrowserName("firefox");
////            dc.setCapability("marionette", true);
////            dc.setPlatform(Platform.LINUX);
////            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), dc);
////            driver = new FirefoxDriver();
//        }
//
//        driver.get("https://www.templatemonster.com/");
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
////        driver.manage().window().maximize();
//        utils = new Utils(driver);
//        mainPage = PageFactory.initElements(driver, MainPage.class);
//        productPage = PageFactory.initElements(driver, ProductPage.class);
//        checkOutPage = PageFactory.initElements(driver, CheckOutPage.class);
//        accountPage = PageFactory.initElements(driver, AccountPage.class);
//
//    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test
    void getCookieTestCase(){
        Cookie expectCookie = utils.getSomeCookie("aff");
        Assert.assertTrue(expectCookie.getValue().equals("TM"), "isn't expected cookie value");
    }

//    @DataProvider
//    Object[][] langs(){
//        return new String[][]{
//                {"FR"},
//                {"UA"},
//        };
//    }

    @Test(priority = 1, dataProvider = "langs", dataProviderClass = StaticProvider.class)
    void languageLocaleTestCase(String language){
        mainPage.selectLanguage(language);
        Assert.assertTrue(utils.checkSomeUrl(language), "it isn't expected URL");
    }

    @Test(priority = 2)
    void signInTestCase(){
        accountPage.signInWithFaceBook();
        Assert.assertTrue(accountPage.checkProfile(), "profile button isn't present");
    }

//    @DataProvider
//    Object[][] initUserCountry(){
//        return new String[][]{
//                {"US"},
//                {"UA"}
//        };
//    }

    @Test(dataProvider = "userCountry", priority = 3, dataProviderClass = StaticProvider.class)
    void CheckOutTestCase(String userCountry) throws IOException {
        productPage.addProductToCart();
        checkOutPage.registerBuyer(new User(userCountry));
        Assert.assertTrue(checkOutPage.getViaPayPalButton().isDisplayed(), "PayPal button isn't present");
    }

//    @AfterMethod()
//    void afterTest() throws IOException {
//        utils.takeScreenShot();
//        driver.quit();
//    }
}