package com.templatemonster.Services;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {
    private WebDriver driver;
    private Wait wait;

    public Utils(WebDriver driver) {
        this.driver = driver;
        wait = new Wait(driver, 10);
    }

    public Cookie getSomeCookie(String nameCookie) {
        wait.waitCookie(nameCookie);
        return driver.manage().getCookieNamed(nameCookie);
    }

    public boolean checkSomeUrl(String language){
        String expectUrl = "https://www.templatemonster.com/" + language.toLowerCase() + "/";
        wait.waitChangeURL(expectUrl);
        return driver.getCurrentUrl().equals(expectUrl);
    }

    public void clickOnElement(WebElement button){
        wait.isClickable(button);
        button.click();
    }

    public void takeScreenShot() throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy_hhmm");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("target/screenshots/screenshot_" + formater.format(calendar.getTime())+ ".png"));
    }

}
