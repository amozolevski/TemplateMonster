package com.templatemonster.Services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {
    private final WebDriverWait wait;

    public Wait(WebDriver driver, long sec) {
        this.wait = new WebDriverWait(driver, sec);
    }

    public void waitHeartIcon(){
        wait.until((WebDriver b) -> b.findElement(By.id("menu-favorites")).isDisplayed());
    }

    public void waitCookie(String cookieName){
        wait.until((WebDriver c) -> c.manage().getCookieNamed(cookieName));
    }

    public void waitChangeURL(String expectUrl) {
        wait.until((WebDriver b) -> b.getCurrentUrl().equals(expectUrl));
    }

    public void isClickable(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void isVisible(WebElement element){
        wait.until((WebDriver webdriver) -> element.isDisplayed());
    }

}