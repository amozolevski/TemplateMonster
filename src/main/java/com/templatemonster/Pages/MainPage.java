package com.templatemonster.Pages;

import com.templatemonster.Services.Utils;
import com.templatemonster.Services.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {
    private final WebDriver driver;
    private final Wait wait;
    private final Utils utils;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
        this.utils = new Utils(driver);
    }

    @FindBy(xpath = "//*[contains(@class,'language-pick')]")
    private WebElement langMenu;

    public void selectLanguage(String language){
        wait.waitHeartIcon();
        String langID = "menu-" + language + "-locale";
        utils.clickOnElement(langMenu);
        utils.clickOnElement(driver.findElement(By.id(langID)));
    }

}