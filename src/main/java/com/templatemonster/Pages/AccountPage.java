package com.templatemonster.Pages;

import com.templatemonster.Services.Utils;
import com.templatemonster.Services.Wait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class AccountPage {
    private WebDriver driver;
    private Wait wait;
    private Utils utils;

    public AccountPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
        this.utils = new Utils(driver);
    }

    @FindBy(id = "header-signin-link")
    private WebElement accountButton;

    @FindBy(id = "id-general-facebook-button")
    private WebElement facebookButton;

    @FindBy(id = "top-panel-to-profile-page-link")
    private WebElement profileLink;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "pass")
    private WebElement password;

    @FindBy(id = "loginbutton")
    private WebElement loginButton;

    public void signInFaceBook(String mail, String pass){
        email.sendKeys(mail);
        password.sendKeys(pass);
        loginButton.click();
    }

    public void signInWithFaceBook(){
        wait.waitHeartIcon();
        String firstHandle = driver.getWindowHandle();
        utils.clickOnElement(accountButton);

        List<String> tabsList = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabsList.get(1));

        String secondHandle = driver.getWindowHandle();

        utils.clickOnElement(facebookButton);

        if(driver.getWindowHandles().size() > 2){
            for (String handle : driver.getWindowHandles()) {
                if(!handle.equals(firstHandle) && !handle.equals(secondHandle)) driver.switchTo().window(handle);
            }
        }

        wait.isVisible(email);
        signInFaceBook("litstest@i.ua", "linux-32");

        driver.switchTo().window(secondHandle);
        wait.isVisible(profileLink);
    }

    public boolean checkProfile(){
        return profileLink.isDisplayed();
    }

}